#!/bin/bash
cd "$(dirname "$0")"
if [[ ! -f venv ]]; then
    virtualenv venv
fi
pwd
. venv/bin/activate
pip install -r $(pwd)/requirements.txt
cd ..
pip install -r $(pwd)/requirements.txt
python -m pytest -rxsv -vvv --instafail --cov=$(pwd) --cov-report term-missing --capture=no tests/
