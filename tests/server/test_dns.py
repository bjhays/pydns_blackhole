# import pytest
# from pytest import request
# import threading
# import time
import unittest
from mock import patch, mock
from dnslib import QTYPE
import logging
import socket
import pprint
import errno
import time
import io

pp = pprint.PrettyPrinter(indent=4)

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("dns.py").setLevel(logging.DEBUG)
log = logging.getLogger(__file__)

import dns as pydns  # pylint: disable=E402 # noqa: E402


DNS_PACKET = '\xc1\x01\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x08coconuts\x03com\x00\x00\x01\x00\x01'
INVALID_DNS_PACKET = '\xc2\x01\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x08coconuts\x03com\x00\x00\x01\x00\x01'
REPLY_NONE = '\xc1\x01\x81\x83\x00\x01\x00\x00\x00\x00\x00\x00\x08coconuts\x03com\x00\x00\x01\x00\x01'
REPLY_FOUND = '\xc1\x01\x81\x80\x00\x01\x00\x01\x00\x00\x00\x00\x08coconuts\x03com\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00\x00<\x00\x04\x7f\x00\x00\x01'  # pylint: disable=E501 # noqa E501


class TestDNS(unittest.TestCase):
    def test_nothing(self):
        return True

    def test_cached(self):
        # test that my.cached.cc A is cached
        # input fqdns, qtype, ip=None
        res = pydns.cached("my.cached.cc", QTYPE[1], "186.186.186.186")
        log.debug(res)
        res2 = pydns.cached("my.cached.cc", QTYPE[1])
        assert res2 == res
        time.sleep(pydns.CACHE_EXPIRE_AGE)
        res3 = pydns.cached("my.cached.cc", QTYPE[1])
        assert res3 is None

    def test_load_block_list_mem(self):
        open_name = 'dns.open'
        d = u'findMe\n'
        fake_file = io.StringIO(d)
        with patch(open_name, return_value=fake_file):
            res = pydns.load_block_list_mem("fake")
            log.debug(res)
            assert d.rstrip() in res

        d = u'findMe\nHiddenHmmmm'
        fake_file = io.StringIO(d)
        with patch(open_name, return_value=fake_file):
            res = pydns.load_block_list_mem("fake")
            log.debug(res)
            for v in d.split("\n"):
                assert v.rstrip() in res

    def test_dns_reply_found(self):
        dns = pydns.DNSQuery(DNS_PACKET)
        log.debug("DNS: %s " % dns)
        reply = dns.build_reply("127.0.0.1")
        log.debug("reply: %s" % reply)
        assert reply == REPLY_FOUND

    def test_dns_reply_not_found(self):
        dns = pydns.DNSQuery(DNS_PACKET)
        log.debug("DNS: %s " % dns)
        reply = dns.build_reply('')
        log.debug("reply: %s" % reply)
        assert reply == REPLY_NONE

    def test_get_ip_address(self):
        assert pydns.get_ip_address() != ""

    def test_mem(self):
        # check that the default MEM coconuts.com
        ip, debug = pydns.get_mem("coconuts.com")
        assert ip == "127.0.0.1"
        assert debug == ["mem_found"]

    def test_get_ip_found(self):
        packet = pydns.DNSQuery(DNS_PACKET)
        ip, debug, blocked = pydns.get_ip(packet)
        log.debug("%s %s %s" % (ip, debug, blocked))
        assert ('127.0.0.1', False) == (ip, blocked)
        assert ['mem_found'] == debug

    def test_get_ip_not_found_external(self):
        packet = pydns.DNSQuery(DNS_PACKET)
        packet.domain = 'abc.com.'
        ip, debug, blocked = pydns.get_ip(packet)
        log.debug("%s %s %s" % (ip, debug, blocked))
        # assert ('127.0.0.1', False) == (ip, blocked)
        assert ['mem_not_found', 'match not found', 'resolving externally'] == debug

    def test_get_ip_not_found_no_external(self):
        packet = pydns.DNSQuery(DNS_PACKET)
        packet.domain = 'abc.com.'
        ip, debug, blocked = pydns.get_ip(packet, False)
        log.debug("%s %s %s" % (ip, debug, blocked))
        # assert ('127.0.0.1', False) == (ip, blocked)
        assert ['mem_not_found', 'match not found', 'return default addr'] == debug

    def test_get_ip_blocked(self):
        with patch('dns.get_mem') as mock_mem:
            packet = pydns.DNSQuery(DNS_PACKET)
            mock_mem.return_value = 'localhost', ["mem_found"]
            ip, debug, blocked = pydns.get_ip(packet, False)
            log.debug("%s %s %s" % (ip, debug, blocked))
            assert ['mem_found', 'localhost return default addr'] == debug

    def test_ext_request(self):
        # socket.gethostbyname
        exp_res = "172.217.9.206"
        with patch('socket.gethostbyname') as mock_socket:
            mock_socket.return_value = exp_res
            res = pydns.ext_request("google.com")
            try:
                assert res == exp_res
            except:
                log.debug(res)
                raise

    def test_invlaid_dns_packet(self):
        # (<class 'socket.gaierror'>, gaierror(8, 'nodename nor servname provided, or not known'), <traceback object at 0x1060193f8>)
        # [Errno 8] nodename nor servname provided, or not known
        with patch('socket.gethostbyname') as mock_socket:
            mock_socket.side_effect = socket.gaierror(8, 'nodename nor servname provided, or not known')
            res = pydns.ext_request('A')
            log.debug("RES: %s" % res)
            assert res == ''

    def test_get_request_info(self):
        info = pydns.get_request_info(DNS_PACKET, ["111.111.111.111"])
        pp.pprint(info)
        exp = {'blocked': False,
               'cache_res': None,
               'debug': ['mem_found'],
               'domain': 'coconuts.com',
               'from_address': "111.111.111.111",
               'ip': '127.0.0.1',
               'is_cached': False,
               'qtype': 'A',
               'reply': REPLY_FOUND,
               }
        for k in exp:
            assert info[k] == exp[k]

        assert info['request'] is not None
        assert info['ts'] is not None
        assert info['start'] is not None
        assert info['p'] is not None

    def test_listen(self):
        with patch('socket.socket') as mock_socket:
            mock_socket.recvfrom.return_value = (DNS_PACKET, ["111.111.111.111"])
            info = pydns.listen(mock_socket)
            expected_calls = [mock.call(REPLY_FOUND, ['111.111.111.111'])]
            mock_socket.sendto.assert_has_calls(expected_calls)
            assert info != {}

    def test_binder(self):
        with patch('socket.socket') as mock_socket:
            udps = pydns.binder('127.0.0.1', 53)
            expected_calls = [mock.call(65535, 512, 1)]
            udps.setsockopt.assert_has_calls(expected_calls)
            expected_calls = [mock.call(('127.0.0.1', 53))]
            udps.bind.assert_has_calls(expected_calls)
            print(mock_socket)

    def test_binder_exception(self):
        with patch('socket.socket.setsockopt') as mock_socket:
            # mock_socket.setsockopt = mock()
            mock_socket.side_effect = socket.error(errno.EADDRINUSE,
                                                   'In use')
            udps = pydns.binder('127.0.0.1', 55555)
            expected_calls = [mock.call(65535, 512, 1)]
            udps.setsockopt.assert_has_calls(expected_calls)
            print(mock_socket)

    def test_main(self):
        with patch('dns.listen') as mock_listen:
            mock_listen.side_effect = KeyboardInterrupt
            assert pydns.main('127.0.0.1', 55553)

    def test_log_request(self):
        # qtype = ""
        # domain = ""
        # from_address = ""
        # ts = ""
        # is_cached = ""
        # ins = ""
        # block_msg = ""
        info = {"domain": "nowhere.com"}
        pydns.log_request(info)
