#!/usr/bin/env python
"""PyTinyDNS docstring.

This script acts as a light A record DNS resolver.
"""
import logging
from dnslib import DNSRecord, QTYPE
import socket
import os
import sys
import time
from datetime import datetime
from expiringdict import ExpiringDict

CACHE_EXPIRE_AGE = 10
CACHE = ExpiringDict(max_len=100, max_age_seconds=CACHE_EXPIRE_AGE)

# this might be necessary in some cases
# try:
socket.SO_REUSEPORT
# except AttributeError:
#     socket.SO_REUSEPORT = 15

logging.basicConfig(format="[%(levelname)s] %(name)s - %(filename)s:%(lineno)d %(message)s")
log = logging.getLogger(__file__)

# Global variables
DEFAULT_IP = '127.0.0.1'
this_server_name = socket.gethostname()
pid = os.getpid()
cache_len = 0
hostnames = {}

MEM_STUFF = {"coconuts.com": "127.0.0.1"}


# DNSQuery class from http://code.activestate.com/recipes/491264-mini-fake-dns-server/
class DNSQuery:
    def __init__(self, data):
        self.data = data
        self.domain = ''

        tipo = (ord(data[2]) >> 3) & 15   # Opcode bits
        if tipo == 0:                     # Standard query
            ini = 12
            lon = ord(data[ini])
            while lon != 0:
                self.domain += data[ini + 1:ini + lon + 1] + '.'
                ini += lon + 1
                lon = ord(data[ini])

    def build_reply(self, ip):
        packet = ''
        if ip == '':  # Taken from crypt0s (https://github.com/Crypt0s/FakeDns/blob/master/fakedns.py)
            # Build the response packet
            packet += self.data[:2] + "\x81\x83"                         # Reply Code: No Such Name, 0 answer rrs   0 additional, 0 auth
            packet += self.data[4:6] + '\x00\x00' + '\x00\x00\x00\x00'   # Questions and Answers Counts
            packet += self.data[12:]                                     # Original Domain Name Question

        if self.domain and packet == '':
            packet += self.data[:2] + "\x81\x80"
            packet += self.data[4:6] + self.data[4:6] + '\x00\x00\x00\x00'   # Questions and Answers Counts
            packet += self.data[12:]                                         # Original Domain Name Question
            packet += '\xc0\x0c'                                             # Pointer to domain name
            packet += '\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04'             # Response type, ttl and resource data length -> 4 bytes
            packet += str.join('', map(lambda x: chr(int(x)), ip.split('.')))  # 4bytes of IP
        return packet


def ext_request(domain):
    # returns ip address in format `172.217.9.206`
    try:
        ret = socket.gethostbyname(domain)
        log.debug("ext_request(%s): %s" % (domain, ret))
        return ret
    except:  # Domain doesn't exist
        print(sys.exc_info())
        print('[-] Unable to parse request %s: %s' % (domain, sys.exc_info()[1]))
        return ''


def cached(fqdns, qtype, ip=None):
    global CACHE
    h = "{}{}".format(fqdns, qtype)
    if ip is not None:
        CACHE[h] = ip
    return CACHE.get(h)


def get_mem(domain):
    log.debug("%s in %s" % (domain, MEM_STUFF))
    if domain in MEM_STUFF:
        return MEM_STUFF[domain], ["mem_found"]
    else:
        return None, ["mem_not_found"]


def load_block_list_mem(filename):
    global MEM_STUFF
    with open(filename) as f:
        for l in f:
            # log.debug(l) # really noisy
            MEM_STUFF[l.rstrip()] = DEFAULT_IP
    return MEM_STUFF


def get_ip(packet, resolve_nonmatch=True):
    domain = (packet.domain).strip(".")
    ip, debug = get_mem(domain)
    blocked = False
    if ip is None:
        debug.append("match not found")
        if resolve_nonmatch is True:
            debug.append("resolving externally")
            ip = ext_request(domain)
        else:
            debug.append("return default addr")
            ip = DEFAULT_IP
    elif ip == "localhost":
        blocked = True
        debug.append("localhost return default addr")
        ip = DEFAULT_IP

    return ip, debug, blocked


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def log_request(info):
    # TODO log to a DB, for now debug to log
    log.debug(info)


def get_request_info(data, src_addr):
    """Parse dns request and build loggable info cache if possible."""
    p = DNSQuery(data)
    request = DNSRecord.parse(data)
    qtype = QTYPE[request.questions[0].qtype]
    info = {"blocked": False,
            "ip": None,
            "is_cached": False,
            "debug": [],
            "start": time.time(),
            "p": p,
            "request": request,
            "qtype": qtype,
            "domain": p.domain.rstrip('.'),
            "from_address": src_addr[0],
            "ts": datetime.now().strftime("%b %d %H:%M:%S"),
            "cache_res": cached(p.domain, qtype, None),
            "reply": None,
            }

    if info['cache_res'] is None:
        # if we didn't find in cache, look it up and cache
        info['ip'], info['debug'], info['blocked'] = get_ip(p)
        cached(p.domain, qtype, info['ip'])
    else:
        info['ip'] = info['cache_res']
        info['is_cached'] = True

    info['reply'] = p.build_reply(info['ip'])
    return info


def listen(udps):
    """Listen for DNS requests and respond."""
    data, src_addr = udps.recvfrom(1024)
    info = get_request_info(data, src_addr)
    udps.sendto(info['reply'], src_addr)
    info['ins'] = time.time() - info['start']
    log_request(info)
    return info


def binder(ip, port):
    """Bind to given port and ip."""
    print '[-] PyTinyDNS starting on {}:{}'.format(ip, port)
    udps = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("Binder %s" % udps)
    # SO_REUSEPORT option allows multiple threads to bind to one port.
    # kernel >= 3.9 https://lwn.net/Articles/542629/
    try:
        udps.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        print("set socket")
    except socket.error:
        print sys.exc_info()
        print '[-] SO_REUSEPORT not supported by your system.'
    print("Bind %s:%s" % (ip, port))
    udps.bind((ip, port))
    return udps


def main(ip, port):
    """The loop."""
    udps = binder(ip, port)
    print '[-] Waiting for request'
    try:
        while 1:
            listen(udps)
    except KeyboardInterrupt:
        print '[-] Ending'
        udps.close()
    return True


if __name__ == '__main__':
    load_block_list_mem("block.list")
    main(get_ip_address(), 53)
