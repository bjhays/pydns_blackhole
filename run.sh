#!/bin/bash

# TODO: add kill trap
cd "$(dirname "$0")"

# clean up so all changes are applied
find . -name \*.pyc -delete;

set -ex
if [[ ! -f venv ]]; then
    virtualenv venv
fi
. venv/bin/activate

# always check for updates
pip install -r requirements.txt

python pytinydns.py
