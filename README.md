# PyDNS_Blackhole

Python based DNS blackhole for blocking ads (advertisements) on all devices using this as a DNS lookup. 

I created this due to a need to have ads blocked on my children's tablets. Some of the apps had very inappropriate ads and others had clickbait. Another issue was that my kids couldn't tell the difference between the app and the ad.

# Requirements
- Python 2.7
- virtualenv

# Block list
`block.list` will be loaded when the server starts. List each fqdns address that you want to block.
note: site.com will not block www.site.com, you must use the fqdns

# RUN
Once you have the above requirements installed, run `./run.sh`.
This will create a virtualenv, activate, install reqs and start the server

# Tests
Unit tests are in the 'test' directory. To run `tests/run.sh`
